'use strict';
/**
 * db config
 * @type {Object}
 */
export default {
  type: 'mysql',
  adapter: {
    mysql: {
      host: '127.0.0.1',
      port: '8889',
      database: 'miu',
      user: 'root',
      password: 'qwer1234',
      prefix: 'm_',
      encoding: 'utf8'
    },
    mongo: {

    }
  }
};