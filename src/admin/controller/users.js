'use strict';

import Base from './base.js';

export default class extends Base {
    /**
     * index action
     * @return {Promise} []
     */
    * indexAction() {
        let model = this.model("users");
        let page = this.isGet() ? this.get('page') : 1;
        let users = yield model.page(page, 8).countSelect();
        this.assign('users', users);
        return this.display();
    }

    * addAction() {
        let data = this.post();
        let model = this.model("users");
        data = { nickname: data.nickname, openid: data.openid, headimgurl: data.headimgurl, sex: data.sex }
        data = this.appendToAddSql(data);
        let inserId = yield model.thenAdd(data, { openid: data.openid });
        if (inserId.id > 0) {
            return this.success('success to add user');
        } else {
            return this.fail('fail to insert, type ' + inserId.type);
        }
    }

    * deleteAction() {
        let id = this.get('id');
        let model = this.model("users");
        let result = yield model.delete({ where: { id: id } });
        if (result > 0) {
            return this.success('success to delete user');
        } else {
            return this.fail('fail to delete');
        }
    }

    * editAction() {
        if (this.isGet()) {
            let id = this.get('id');
            let model = this.model("users");
            let user = yield model.where({ id: id }).find();
            if (!!user) {
                this.assign('user', user);
                return this.display();
            }
        }
    }

    * editSubmitAction() {
        if (this.isPost()) {
            let id = this.post('id');
            let data = this.post();
            let model = this.model("users");
            data = { nickname: data.nickname, openid: data.openid, headimgurl: data.headimgurl, sex: data.sex }
            data = this.appendToSaveSql(data);
            console.log(data);
            let user = yield model.where({ id: id }).update(data);
            if (user) {
                return this.success('success to edit user');
            } else {
                return this.fail('fail to edit');
            }
        }
    }
}
