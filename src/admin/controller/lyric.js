'use strict';

import Base from './base.js';

export default class extends Base {
    /**
     * index action
     * @return {Promise} []
     */
    * indexAction() {
        let model = this.model("lyric");
        let page = this.isGet() ? this.get('page') : 1;
        let lyric = yield model.page(page, 8).countSelect();
        // console.log(lyric);
        this.assign('lyric', lyric);
        return this.display();
    }

    * addAction() {
        let data = this.post();
        let model = this.model("lyric");
        data = { user_id: 1, lyric_title: data.lyric_title, lyric: data.lyric }
        data = this.appendToAddSql(data);
        let inserId = yield model.thenAdd(data, { lyric_title: data.lyric_title });
        if (inserId.id > 0) {
            return this.success('success to add lyric');
        } else {
            return this.fail('fail to insert, type ' + inserId.type);
        }
    }

    * deleteAction() {
        let id = this.get('id');
        let model = this.model("lyric");
        let result = yield model.delete({ where: { id: id } });
        if (result > 0) {
            return this.success('success to delete lyric');
        } else {
            return this.fail('fail to delete');
        }
    }

    * editAction() {
        if (this.isGet()) {
            let id = this.get('id');
            let model = this.model("lyric");
            let lyric = yield model.where({ id: id }).find();
            if (!!lyric) {
                this.assign('lyric', lyric);
                return this.display();
            }
        }
    }

    * editSubmitAction() {
        if (this.isPost()) {
            let id = this.post('id');
            let data = this.post();
            let model = this.model("lyric");
            console.log(data.lyric_title);
            data = { lyric_title: data.lyric_title, lyric: data.lyric }
            data = this.appendToSaveSql(data);
            let lyric = yield model.where({ id: id }).update(data);
            if (lyric) {
                return this.success('success to edit lyric');
            } else {
                return this.fail('fail to edit');
            }
        }
    }
}
