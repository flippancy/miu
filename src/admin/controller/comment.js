'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */
    * indexAction() {
        let model = this.model("comment");
        let page = this.isGet() ? this.get('page') : 1;
        let comment = yield model.page(page, 8).countSelect();
        console.log(comment);
        this.assign('comment', comment);
        return this.display();
    }

    * deleteAction() {
        let id = this.get('id');
        let model = this.model("comment");
        let result = yield model.delete({ where: { id: id } });
        if (result > 0) {
            return this.success('success to delete comment');
        } else {
            return this.fail('fail to delete');
        }
    }

    * editAction() {
        if (this.isGet()) {
            let id = this.get('id');
            let model = this.model("comment");
            let comment = yield model.where({ id: id }).find();
            if (!!comment) {
                this.assign('comment', comment);
                return this.display();
            }
        }
    }

    * editSubmitAction() {
        if (this.isPost()) {
            let id = this.post('id');
            let data = this.post();
            let model = this.model("comment");
            data = { comment_content: data.comment_content }
            data = this.appendToSaveSql(data);
            console.log(data);
            let comment = yield model.where({ id: id }).update(data);
            if (comment) {
                return this.success('success to edit comment');
            } else {
                return this.fail('fail to edit');
            }
        }
    }

    * changeStatusAction() {
        if (this.isGet()) {
            let id = this.get('id');
            let data = { status: this.get('status') };
            data = this.appendToSaveSql(data);
            let model = this.model("comment");
            let comment = yield model.where({ id: id }).update(data);
            if (comment) {
                return this.success('success to edit comment');
            } else {
                return this.fail('fail to edit');
            }
        }
    }
}