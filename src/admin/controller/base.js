'use strict';

export default class extends think.controller.base {
  /**
   * some base method in here
   */
  async __before(){
    //部分 action 下不检查
    let blankActions = ["login"];
    if(blankActions.indexOf(this.action)){
      return;
    }
    let userInfo = await this.session("userInfo");
    //判断 session 里的 userInfo
    if(think.isEmpty(userInfo)){
      return this.redirect("/index/login");
    }
  }

  appendToAddSql(data){
  	let time = (this.http.startTime).toString().substr(0, 10);
  	time = new Date(parseInt(time) * 1000).toLocaleString();

  	data.created_at = time;
  	data.created_by = this.http.controller + '/' + this.http.action;
  	data.updated_at = time;
  	data.updated_by = this.http.controller + '/' + this.http.action;
  	return data;
  }

  appendToSaveSql(data){
  	let time = (this.http.startTime).toString().substr(0, 10);
  	time = new Date(parseInt(time) * 1000).toLocaleString();
  	
  	data.updated_at = time;
  	data.updated_by = this.http.controller + '/' + this.http.action;
  	return data;
  }
}