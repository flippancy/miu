'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */
    * indexAction() {
        let model = this.model("music");
        let page = this.isGet() ? this.get('page') : 1;
        let music = yield model.page(page, 8).countSelect();
        console.log(music);
        this.assign('music', music);
        return this.display();
    }

    * deleteAction() {
        let id = this.get('id');
        let model = this.model("music");
        let result = yield model.delete({ where: { id: id } });
        if (result > 0) {
            return this.success('success to delete music');
        } else {
            return this.fail('fail to delete');
        }
    }

    * editAction() {
        if (this.isGet()) {
            let id = this.get('id');
            let model = this.model("music");
            let music = yield model.where({ id: id }).find();
            if (!!music) {
                this.assign('music', music);
                return this.display();
            }
        }
    }

    * editSubmitAction() {
        if (this.isPost()) {
            let id = this.post('id');
            let data = this.post();
            let model = this.model("music");
            data = { music_title: data.music_title, music_url: data.music_url, music_note: data.music_note }
            data = this.appendToSaveSql(data);
            console.log(data);
            let music = yield model.where({ id: id }).update(data);
            if (music) {
                return this.success('success to edit music');
            } else {
                return this.fail('fail to edit');
            }
        }
    }
}