'use strict';
/**
 * relation model
 */
export default class extends think.model.relation {
  /**
   * init
   * @param  {} args []
   * @return {}         []
   */
  init(...args){
    super.init(...args);

    this.relation = {
      users:{
        type:think.model.BELONG_TO,
        key: "user_id",
        fKey: "id",
        field: "id,nickname"
      },
    };
  }
}