'use strict';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
  /**
   * index action logic
   * @return {} []
   */
  editSubmitAction(){
	this.allowMethods = "post";
	this.rules = {
		id: "required|int",
		comment_content: "required|string",
	}
  }
}