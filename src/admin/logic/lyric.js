'use strict';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
  /**
   * index action logic
   * @return {} []
   */
  addAction(){
	this.allowMethods = "post";
	this.rules = {
		lyric_title: "required|string",
		lyric: "required|string",
	}
  }

  editSubmitAction(){
	this.allowMethods = "post";
	this.rules = {
		id: "required|int",
		lyric_title: "required|string",
		lyric: "required|string",
	}
  }

}