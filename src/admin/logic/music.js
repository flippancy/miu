'use strict';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
  /**
   * index action logic
   * @return {} []
   */
  indexAction(){
   
  }

  editSubmitAction(){
	this.allowMethods = "post";
	this.rules = {
		id: "required|int",
		music_title: "required|string",
		music_url: "required|string",
		music_note: "required|string",
	}
  }
}