'use strict';
/**
 * logic
 * @param  {} []
 * @return {}     []
 */
export default class extends think.logic.base {
  /**
   * index action logic
   * @return {} []
   */
  addAction(){
	this.allowMethods = "post";
	this.rules = {
		nickname: "required|string",
		openid: "required|string",
		headimgurl: "required|url",
		sex: "required|int:1,2",
	}
  }

  editSubmitAction(){
	this.allowMethods = "post";
	this.rules = {
		id: "required|int",
		nickname: "required|string",
		openid: "required|string",
		headimgurl: "required|url",
		sex: "required|int:1,2",
	}
  }

}