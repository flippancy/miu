'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */
  * indexAction(){
    return this.display();
  }

  * recordAction(){
  	return this.display();
  }

  * checkAction(){
    let record_code = this.post('record_code');
    let record = yield this.model("record").where({ record_code: record_code}).field('user_id').find();
    console.log(record);
    return this.success(record);
  }

  * getTokenAction(){
    let qiniu = require("qiniu");

    //需要填写你的 Access Key 和 Secret Key
    qiniu.conf.ACCESS_KEY = think.config('qiniu').ACCESS_KEY;
    qiniu.conf.SECRET_KEY = think.config('qiniu').SECRET_KEY;

    //要上传的空间
    let bucket = think.config('qiniu').Bucket_Name;

    //上传到七牛后保存的文件名
    let key = this.get('file_name');

    //构建上传策略函数
    let putPolicy = new qiniu.rs.PutPolicy(bucket+":"+key);

    //生成上传 Token
    let token = putPolicy.token();

    return this.success(token);
  }
}