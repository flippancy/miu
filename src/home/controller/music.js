'use strict';
/**
 * rest controller
 * @type {Class}
 */
export default class extends think.controller.rest {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  __before(){
    this.http.header("Access-Control-Allow-Origin",'*');
  }

  * getAction(){
    let data;
    if (this.id) {
      let pk = yield this.modelInstance.getPk();
      data = yield this.modelInstance.where({[pk]: this.id}).find();
      return this.success(data);
    }
    let date = this.get('created_at');
    data = yield this.modelInstance.where({created_at: {"<": date + ' 23:59:59', ">": date + ' 00:00:00', _logic: "AND"}}).select();
    for (var i = data.length - 1; i >= 0; i--) {
      let user_id = this.get('User_id');
      let like = yield this.model("like").where({ song_id: data[i].id, user_id: user_id }).field('id').find();
      if (like.id != undefined) {
        data[i].like = true;
      }else{
        data[i].like = false;
      }
    }
    return this.success(data);
  }
}