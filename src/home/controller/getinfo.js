'use strict';

import Base from './base.js';

export default class extends Base {
  /**
   * index action
   * @return {Promise} []
   */
  __before(){
    this.http.header("Access-Control-Allow-Origin",'*');
  }
  
  indexAction(){
    //auto render template file index_index.html
    return this.display();
  }

  * getMusicByUserIdAction(){
    let User_id = this.get('User_id');
    let page = this.isGet() ? this.get('page') : 1;
    let model = this.model("music");
    let music = yield model.where({user_id: User_id}).page(page, 8).countSelect();
    if (!!music) {

      for (let i = music.data.length - 1; i >= 0; i--) {
        let like = yield this.model("like").where({ song_id: music.data[i].id, user_id: User_id }).field('id').find();
        if (like.id != undefined) {
          music.data[i].like = true;
        }else{
          music.data[i].like = false;
        }
      }

      return this.success(music);
    } else {
      return this.fail('fail to find');
    }
  }

  * getLikeMusicByUserIdAction(){
    let User_id = this.get('User_id');
    let page = this.isGet() ? this.get('page') : 1;
    let model = this.model("like");
    let music = yield model
      .join(['m_music on m_like.song_id = m_music.id'])
      .join(['m_users on m_music.user_id = m_users.id'])
      .where({'m_like.user_id': User_id}).page(page, 8).field().countSelect();

    for (let i = music.data.length - 1; i >= 0; i--) {
        music.data[i].like = true;
    }
    if (!!music) {
      return this.success(music);
    } else {
      return this.fail('fail to find');
    }
  }

  * getCommentByMusicIdAction(){
    let Music_id = this.get('Music_id');
    let page = this.isGet() ? this.get('page') : 1;
    let model = this.model("comment");
    let comment = yield model.where({music_id: Music_id}).page(page, 8).order(["created_at DESC"]).countSelect();
    if (!!comment) {
      console.log(comment);
      return this.success(comment);
    } else {
      return this.fail('fail to find');
    }
  }

  * getTopCommentByMusicIdAction(){
    let Music_id = this.get('Music_id');
    let model = this.model("comment");
    let topComment = yield model.where({music_id: Music_id}).order(["likes DESC"]).limit(3).select();
    if (!!topComment) {
      return this.success(topComment);
    } else {
      return this.fail('fail to find');
    }
  }

  * upLikeAction(){
    let song_id = this.get('Song_id');
    let user_id = this.get('User_id');
    this.model("music").where({id: song_id}).increment('like_num');
    let like = yield this.model("like").add({song_id: song_id, user_id: user_id});
    if (!!like) {
        return this.success(like);
    } else {
        return this.fail('fail to find');
    }
  }

  * downLikeAction(){
    let song_id = this.get('Song_id');
    let user_id = this.get('User_id');
    this.model("music").where({id: song_id}).decrement('like_num');
    let like = yield this.model("like").delete({ where:{song_id: song_id, user_id: user_id} });
    if (!!like) {
        return this.success(like);
    } else {
        return this.fail('fail to find');
    }
  }

  * upCommentLikeAction(){
    let id = this.get('id');
    let result = yield this.model("comment").where({id: id}).increment('likes');
    if (!!result) {
        return this.success(result);
    } else {
        return this.fail('fail to find');
    }
  }

  * downCommentLikeAction(){
    let id = this.get('id');
    let result = yield this.model("comment").where({id: id}).decrement('likes');
    if (!!result) {
      return this.success(result);
    } else {
      return this.fail('fail to find');
    }
  }

  * getMusicFileByMusicIdAction(){
    let id = this.get('Music_id');
    this.model("music").where({id: id}).increment('download_num');
    let result = yield this.model("music").where({id: id}).find();
      console.log(result['music_url']);
    if(!!result){
      return this.download(result['music_url']);
    }else{
      return this.fail('fail to download');
    }
  }

  * getTokenAction(){
    let createNonceStr = function () {
      return Math.random().toString(36).substr(2, 15);
    };

    let createTimestamp = function () {
      return parseInt(new Date().getTime() / 1000) + '';
    };

    let raw = function (args) {
      let keys = Object.keys(args);
      keys = keys.sort()
      let newArgs = {};
      keys.forEach(function (key) {
        newArgs[key.toLowerCase()] = args[key];
      });

      let string = '';
      for (let k in newArgs) {
        string += '&' + k + '=' + newArgs[k];
      }
      string = string.substr(1);
      return string;
    };
    let sign = function (jsapi_ticket,url) {
      let ret = {
        jsapi_ticket: jsapi_ticket,
        nonceStr: createNonceStr(),
        timestamp: createTimestamp(),
        url: 'http://flippancy.ngrok.cc/home/index/index'
      };
      let string = raw(ret);
      let jsSHA = require('jssha');
      let shaObj = new jsSHA(string, 'TEXT');
      ret.signature = shaObj.getHash('SHA-1', 'HEX');
      return ret;
    };

    let request = require('request');
    let result = {};

    result['access_token'] = yield this.cache("access_token");
    if(result['access_token'] == undefined){
      let that = this;
      request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
        + think.config('wechat').appID +"&secret="+ think.config('wechat').appsecret, function(error, response, body){
        body = JSON.parse(body);
        that.cache("access_token",body.access_token);
        result['access_token'] = body.access_token;
      })
    }

    result['ticket'] = yield this.cache("ticket");
    if(result['ticket'] == undefined){
      let that = this;
      request("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token="+result['access_token']+"&type=jsapi",
      function(error, response, body){
        body = JSON.parse(body);
        that.cache("ticket",body.ticket);
        result['ticket'] = body.ticket;
      });
    }

    return this.success(sign(result['ticket'], this.http.url));
  }

  * uploadVoiceAction(){
    let data = this.post();
    let request = require('request');
    let fs = require('fs');

    let result = {};
    result['access_token'] = yield this.cache("access_token");
    if(result['access_token'] == undefined){
      let that = this;
      request("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
        + think.config('wechat').appID +"&secret="+ think.config('wechat').appsecret, function(error, response, body){
        body = JSON.parse(body);
        that.cache("access_token",body.access_token);
        result['access_token'] = body.access_token;
      })
    }

    function downloadFile(uri,filename,callback){
        if(!fs.exists('www/static/music/'+today)){
          fs.mkdir('www/static/music/'+today);
        }  
        let stream = fs.createWriteStream(filename);
        request(uri).pipe(stream).on('close', callback); 
    }

    let today = new Date();
    let month,date;
    month = today.getMonth()+1;
    if(today.getMonth() < 10){
      month = "0" + month;
    }
    if(today.getDate() < 10){
      date = "0" + today.getDate();
    }else{
      date = today.getDate();
    }
    today = today.getFullYear() + "-" + month + "-" + date;

    let serverId = data.music_url;
    let fileUrl = "https://api.weixin.qq.com/cgi-bin/media/get?access_token="+result['access_token']+"&media_id="+data.music_url;
    let filename = 'www/static/music/'+today+'/'+data.music_url+'.amr';
    let that = this;
    downloadFile(fileUrl,filename,function(){
      console.log(filename+'下载完毕');
      data.music_url = '/static/music/'+today+'/'+data.music_url+'.amr';
      uploadToQiNiu(data,serverId);
      return that.success('上传发布完毕');
    });

    function uploadToQiNiu(data,serverId){
      let qiniu = require("qiniu");

      //需要填写你的 Access Key 和 Secret Key
      qiniu.conf.ACCESS_KEY = think.config('qiniu').ACCESS_KEY;
      qiniu.conf.SECRET_KEY = think.config('qiniu').SECRET_KEY;

      //要上传的空间
      let bucket = think.config('qiniu').Bucket_Name;

      //上传到七牛后保存的文件名
      let key = serverId+'.mp3';

      //转码是使用的队列名称。 
      let pipleline = 'amrToMp3';

      //要进行转码的转码操作。 
      let fops = "avthumb/mp3";

      //可以对转码后的文件进行使用saveas参数自定义命名，当然也可以不指定文件会默认命名并保存在当间。
      let saveas_key = qiniu.util.urlsafeBase64Encode(bucket+":"+key); 
      fops = fops+'|saveas/'+saveas_key;

      //构建上传策略函数
      function uptoken(bucket, key) {
        let putPolicy = new qiniu.rs.PutPolicy(bucket+":"+key);
        // putPolicy.callbackUrl = 'http://your.domain.com/callback';
        // putPolicy.callbackBody = 'filename=$(fname)&filesize=$(fsize)';

        putPolicy.persistentOps = fops;
        putPolicy.persistentPipeline = pipleline;
        return putPolicy.token();
      }

      //生成上传 Token
      let token = uptoken(bucket, key);

      //要上传文件的本地路径
      let filePath = 'www'+data.music_url;

      //构造上传函数
      function uploadFile(uptoken, key, localFile) {
        let extra = new qiniu.io.PutExtra();
          qiniu.io.putFile(uptoken, key, localFile, extra, function(err, ret) {
            if(!err) {
              // 上传成功， 处理返回值
              console.log(ret.hash, ret.key, ret.persistentId);
              let model = that.model("music");
              data.music_url = 'http://7xl0od.com1.z0.glb.clouddn.com/'+ret.key;
              model.add(data); 
            } else {
              // 上传失败， 处理返回代码
              console.log(err);
            }
        });
      }

      //调用uploadFile上传
      uploadFile(token, key, filePath);
    }
  }
}