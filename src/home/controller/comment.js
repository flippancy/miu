'use strict';
/**
 * rest controller
 * @type {Class}
 */
export default class extends think.controller.rest {
  /**
   * init
   * @param  {Object} http []
   * @return {}      []
   */
  init(http){
    super.init(http);
  }
  /**
   * before magic method
   * @return {Promise} []
   */
  __before(){
    this.http.header("Access-Control-Allow-Origin",'*');
    this.http.header("Access-Control-Allow-Headers",'Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With');
  }

  * postAction(){
    let pk = yield this.modelInstance.getPk();
    let data = this.post();
    delete data[pk];
    if(think.isEmpty(data)){
      return this.fail("data is empty");
    }
    let insertId = yield this.modelInstance.add(data);
    this.model("music").where({id: data.music_id}).increment('comment_num');
    return this.success({id: insertId});
  }
}