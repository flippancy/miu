-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: miu
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `m_comment`
--

DROP TABLE IF EXISTS `m_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '微信openid',
  `music_id` int(11) NOT NULL COMMENT '音乐id',
  `comment_content` varchar(512) NOT NULL COMMENT '评论内容',
  `likes` int(11) NOT NULL DEFAULT '0',
  `status` varchar(8) NOT NULL DEFAULT 'ACT' COMMENT '评论状态 ACT-有效 TRM-无效',
  `parent_id` int(11) DEFAULT NULL COMMENT '被评论id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `updated_at` datetime DEFAULT NULL COMMENT '修改时间',
  `updated_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_comment`
--

LOCK TABLES `m_comment` WRITE;
/*!40000 ALTER TABLE `m_comment` DISABLE KEYS */;
INSERT INTO `m_comment` VALUES (1,1,1,'asd',151,'ACT',NULL,'2016-05-19 09:44:31','comment/index','2016-05-19 10:41:39','comment/change_status'),(2,1,1,'asd',1,'ACT',1,'2016-06-29 00:00:00','rilegou',NULL,NULL),(9,1,1,'asds',2,'ACT',NULL,'2016-06-30 14:01:37','rilegou',NULL,NULL),(10,1,1,'nihao',3,'ACT',NULL,'2016-06-30 14:34:12','rilegou',NULL,NULL),(11,1,1,'撒打算',4,'ACT',13,'2016-06-30 14:34:39','rilegou',NULL,NULL),(12,1,1,'你好',7,'ACT',NULL,'2016-06-30 14:35:34','rilegou',NULL,NULL),(13,1,1,'啊实打实的叫阿卡撒打算打算了大家拉萨大家阿里斯顿克赖斯基的',66,'ACT',NULL,'2016-06-30 14:36:38','rilegou',NULL,NULL),(14,1,1,'sdalsdalsdsd',0,'ACT',NULL,'2016-06-30 16:33:00','rilegou',NULL,NULL),(15,1,1,'asdasdas',0,'ACT',NULL,'2016-06-30 16:33:02','rilegou',NULL,NULL),(16,1,1,'阿萨数量扩大拉力赛',0,'ACT',NULL,'2016-06-30 16:33:11','rilegou',NULL,NULL),(17,1,1,'撒大量时间阿拉斯加',0,'ACT',NULL,'2016-06-30 16:33:14','rilegou',NULL,NULL),(18,1,1,'sdsadasdasd',0,'ACT',NULL,'2016-06-30 17:12:49','rilegou',NULL,NULL),(19,1,1,'asdsad',0,'ACT',NULL,'2016-06-30 17:12:50','rilegou',NULL,NULL),(20,1,1,'asdasd',0,'ACT',NULL,'2016-06-30 17:12:51','rilegou',NULL,NULL),(21,1,1,'asdasdssdqwe',0,'ACT',NULL,'2016-06-30 17:12:53','rilegou',NULL,NULL),(22,1,1,'asdwqwsad',0,'ACT',NULL,'2016-06-30 17:12:55','rilegou',NULL,NULL),(23,1,1,'asdwqewqw',4,'ACT',NULL,'2016-06-30 17:12:57','rilegou',NULL,NULL),(24,1,1,'asdasdad',2,'ACT',NULL,'2016-06-30 17:13:11','rilegou',NULL,NULL),(25,1,1,'asdasds',2,'ACT',NULL,'2016-06-30 17:13:13','rilegou',NULL,NULL),(26,1,1,'qweqweqwe',2,'ACT',NULL,'2016-06-30 17:13:14','rilegou',NULL,NULL),(27,1,1,'qweqwe',3,'ACT',NULL,'2016-06-30 17:13:15','rilegou',NULL,NULL),(28,1,1,'123123',4,'ACT',NULL,'2016-06-30 17:13:20','rilegou',NULL,NULL),(29,1,1,'1231312312',5,'ACT',NULL,'2016-06-30 17:13:21','rilegou',NULL,NULL),(30,1,1,'123123',5,'ACT',NULL,'2016-06-30 17:13:23','rilegou',NULL,NULL),(31,1,2,'asd',0,'ACT',NULL,'2016-07-01 14:14:43','rilegou',NULL,NULL),(32,1,2,'',0,'ACT',NULL,'2016-07-01 14:14:44','rilegou',NULL,NULL),(33,1,2,'1',0,'ACT',NULL,'2016-07-01 14:14:48','rilegou',NULL,NULL),(34,1,2,'2',0,'ACT',NULL,'2016-07-01 14:14:49','rilegou',NULL,NULL),(35,1,2,'3',0,'ACT',NULL,'2016-07-01 14:14:50','rilegou',NULL,NULL),(36,1,2,'4',0,'ACT',NULL,'2016-07-01 14:14:51','rilegou',NULL,NULL),(37,1,2,'5',0,'ACT',NULL,'2016-07-01 14:14:53','rilegou',NULL,NULL),(38,1,2,'6',0,'ACT',NULL,'2016-07-01 14:14:54','rilegou',NULL,NULL),(39,1,2,'7',0,'ACT',NULL,'2016-07-01 14:14:56','rilegou',NULL,NULL),(40,1,2,'8',0,'ACT',NULL,'2016-07-01 14:14:57','rilegou',NULL,NULL),(41,1,2,'9',0,'ACT',NULL,'2016-07-01 14:14:59','rilegou',NULL,NULL),(42,1,2,'123',0,'ACT',NULL,'2016-07-01 14:15:02','rilegou',NULL,NULL),(43,1,2,'111',0,'ACT',NULL,'2016-07-01 14:15:03','rilegou',NULL,NULL),(44,1,2,'222',0,'ACT',NULL,'2016-07-01 14:15:04','rilegou',NULL,NULL),(45,1,2,'333',0,'ACT',NULL,'2016-07-01 14:15:06','rilegou',NULL,NULL),(46,1,1,'sadasd',0,'ACT',1,'2016-07-02 11:25:09','rilegou',NULL,NULL),(47,1,1,'sadasd',0,'ACT',1,'2016-07-02 11:25:12','rilegou',NULL,NULL),(48,1,1,'sadasdasdasd',1,'ACT',12,'2016-07-02 11:25:30','rilegou',NULL,NULL),(49,1,1,'不好',0,'ACT',12,'2016-07-02 11:29:05','rilegou',NULL,NULL),(50,1,1,'jjjnjnjn',0,'ACT',20,'2016-07-04 12:17:37','rilegou',NULL,NULL);
/*!40000 ALTER TABLE `m_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_like`
--

DROP TABLE IF EXISTS `m_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `song_id` int(11) NOT NULL COMMENT '歌曲id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_like`
--

LOCK TABLES `m_like` WRITE;
/*!40000 ALTER TABLE `m_like` DISABLE KEYS */;
INSERT INTO `m_like` VALUES (14,2,1),(19,1,1),(21,3,1);
/*!40000 ALTER TABLE `m_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_lyric`
--

DROP TABLE IF EXISTS `m_lyric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_lyric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `cover_num` int(11) NOT NULL DEFAULT '0' COMMENT '使用次数',
  `lyric_title` varchar(64) NOT NULL DEFAULT '' COMMENT '歌曲名',
  `lyric` text NOT NULL COMMENT '歌词',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(64) DEFAULT NULL COMMENT '创建控制器',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(64) DEFAULT NULL COMMENT '更新控制器',
  PRIMARY KEY (`id`),
  KEY `lyric_user_id` (`user_id`),
  CONSTRAINT `lyric_user_id` FOREIGN KEY (`user_id`) REFERENCES `m_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_lyric`
--

LOCK TABLES `m_lyric` WRITE;
/*!40000 ALTER TABLE `m_lyric` DISABLE KEYS */;
INSERT INTO `m_lyric` VALUES (2,1,0,'asdasd','1 1\n12 12\n123 123\n1234 1234\n12345\n123456\n1234567\n12345678','2016-05-19 08:59:20','lyric/add','2016-05-19 08:59:20','lyric/add'),(3,1,0,'qwe','qwe\nwer\nert\nrty','2016-05-19 09:06:48','lyric/add','2016-05-19 09:08:34','lyric/edit_submit'),(4,1,0,'Faded','Faded - Alan Walker\nYou were the shadow to my light\nDid you feel us\nAnother start\nYou fade away\nAfraid our aim is out of sight\nWanna see us\nAlive\nWhere are you now\nWhere are you now\nWhere are you now\nWas it all in my fantasy\nWhere are you now\nWere you only imaginary\nWhere are you now\nAtlantis\nUnder the sea\nUnder the sea\nWhere are you now','2016-05-20 16:49:41','lyric/add','2016-05-20 17:39:44','lyric/edit_submit');
/*!40000 ALTER TABLE `m_lyric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_music`
--

DROP TABLE IF EXISTS `m_music`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_music` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `lyric_id` int(11) DEFAULT NULL COMMENT '歌词id',
  `music_title` varchar(64) NOT NULL COMMENT '音乐名称',
  `music_url` varchar(255) NOT NULL COMMENT '音乐地址',
  `music_note` varchar(255) NOT NULL COMMENT '用户备注',
  `like_num` int(11) NOT NULL DEFAULT '0' COMMENT '点赞次数',
  `listen_num` int(11) NOT NULL DEFAULT '0' COMMENT '播放次数',
  `comment_num` int(11) NOT NULL DEFAULT '0' COMMENT '评论次数',
  `share_num` int(11) NOT NULL DEFAULT '0' COMMENT '分享次数',
  `download_num` int(11) NOT NULL DEFAULT '0' COMMENT '下载次数',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(64) DEFAULT NULL COMMENT '创建控制器',
  `updated_at` datetime DEFAULT NULL COMMENT '更新时间',
  `updated_by` varchar(64) DEFAULT NULL COMMENT '更新控制器',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `m_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_music`
--

LOCK TABLES `m_music` WRITE;
/*!40000 ALTER TABLE `m_music` DISABLE KEYS */;
INSERT INTO `m_music` VALUES (1,1,2,'Faded','/static/music/Conor Maynard,Alan Walker,ANTH - Faded (Conor Maynard Cover).mp3','muisc_note111',2,11,29,11,40,'2016-06-14 12:53:32','music/edit_submit','2016-05-23 16:00:18','music/edit_submit'),(2,1,3,'Boom Clap','/static/music/Charli XCX - Boom Clap.mp3','muisc_note222',1,22,37,22,22,'2016-06-14 16:40:07','music/edit_submit','2016-05-23 16:00:11','music/edit_submit'),(3,1,4,'Grenade','/static/music/Michael Henry,Justin Robinett - Grenade.mp3','muisc_note333',1,33,33,33,33,'2016-06-14 16:40:07','music/edit_submit','2016-05-23 16:01:05','music/edit_submit'),(18,1,NULL,'何厚铧','http://7xl0od.com1.z0.glb.clouddn.com/CkAeF96P6g7CKnhfWSZJvuH42yq9T0S4awGlbbYbTlcB-aZmnmhTiCD1i6BAD4o0.mp3','工业化',0,0,0,0,0,'2016-07-07 15:59:47',NULL,NULL,NULL);
/*!40000 ALTER TABLE `m_music` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_record`
--

DROP TABLE IF EXISTS `m_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `record_code` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_record`
--

LOCK TABLES `m_record` WRITE;
/*!40000 ALTER TABLE `m_record` DISABLE KEYS */;
INSERT INTO `m_record` VALUES (1,1,'431jhYWTW6DJFIyYDC6j3hjOHL3YLiuZYzUqG');
/*!40000 ALTER TABLE `m_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_rss`
--

DROP TABLE IF EXISTS `m_rss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_rss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '歌曲id',
  `to_user_id` int(11) NOT NULL COMMENT '被订阅用户id',
  `created_at` datetime DEFAULT NULL COMMENT '创建时间',
  `created_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `updated_at` datetime DEFAULT NULL COMMENT '修改时间',
  `updated_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_rss`
--

LOCK TABLES `m_rss` WRITE;
/*!40000 ALTER TABLE `m_rss` DISABLE KEYS */;
/*!40000 ALTER TABLE `m_rss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `m_users`
--

DROP TABLE IF EXISTS `m_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(64) NOT NULL COMMENT '昵称',
  `openid` varchar(64) NOT NULL COMMENT '微信openid',
  `headimgurl` varchar(256) NOT NULL COMMENT '微信头像',
  `sex` tinyint(4) NOT NULL DEFAULT '0' COMMENT '用户的性别，值为1时是男性，值为2时是女性，值为0时是未知',
  `listen_num` int(11) DEFAULT NULL COMMENT '听歌次数',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `m_users`
--

LOCK TABLES `m_users` WRITE;
/*!40000 ALTER TABLE `m_users` DISABLE KEYS */;
INSERT INTO `m_users` VALUES (1,'rilegou','rilegou','https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=622578697,1657698698&fm=116&gp=0.jpg',1,NULL,'2016-05-18 10:53:05','users/add','2016-05-27 10:58:34','users/edit_submit');
/*!40000 ALTER TABLE `m_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-08 10:07:31
